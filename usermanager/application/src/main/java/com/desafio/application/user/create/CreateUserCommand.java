package com.desafio.application.user.create;

public record CreateUserCommand(String name, String cpf) {
  public static CreateUserCommand with(String name, String cpf) {
    return new CreateUserCommand(name, cpf);
  }

}
