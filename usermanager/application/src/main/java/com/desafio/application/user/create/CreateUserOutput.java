package com.desafio.application.user.create;

import com.desafio.domain.user.User;
import com.desafio.crosscutting.notification.INotification;

public record CreateUserOutput(String name, String cpf, INotification notificationErrors) {

  public static CreateUserOutput from(User user) {
    return new CreateUserOutput(user.getName(), user.getCpf().getValue(), null);
  }

  public static CreateUserOutput from(INotification notification) {
    return new CreateUserOutput(null, null, notification);
  }

  public CreateUserOutput(String name, String cpf) {
    this(name, cpf, null);
  }
}
