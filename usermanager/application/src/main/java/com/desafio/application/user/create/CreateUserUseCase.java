package com.desafio.application.user.create;

import com.desafio.application.user.findByCPF.FindByCPFUseCase;
import com.desafio.application.user.findByCPF.FindByCPFCommand;
import com.desafio.application.utils.Constants;
import com.desafio.crosscutting.notification.Notification;
import com.desafio.crosscutting.notification.NotificationErrorProps;
import com.desafio.domain.user.User;
import com.desafio.domain.user.UserGateway;
import com.desafio.crosscutting.log.ILog;
import com.desafio.crosscutting.log.Log;
import com.desafio.crosscutting.notification.INotification;

import java.util.Objects;

public class CreateUserUseCase {

  private static final ILog log = new Log(CreateUserUseCase.class);

  private final UserGateway userGateway;

  private INotification notification;

  private CreateUserUseCase(UserGateway userGateway) {
    this.userGateway = Objects.requireNonNull(userGateway);
  }

  public static CreateUserUseCase create(UserGateway userGateway) {
    return new CreateUserUseCase(userGateway);
  }

  public CreateUserOutput execute(CreateUserCommand createUserCommand) {
    User user = this.createUser(createUserCommand);

    user.validate();

    if (user.getNotification().hasErrors())
      return CreateUserOutput.from(user.getNotification());

    log.info(Constants.USER_VALIDATED, user.getName());

    if (this.verifyIfUserExists(user.getCpf().getValue())) {
      log.info(Constants.USER_ALREADY_EXISTS, user.getCpf().getValue());
      notification = new Notification();
      notification.append(new NotificationErrorProps(Constants.USER_ALREADY_EXISTS, Constants.CLASS_STR));
      return CreateUserOutput.from(new Notification());
    }

    CreateUserOutput output = CreateUserOutput.from(this.userGateway.create(user));
    log.info("user with cpf: '{}' successfully inserted on gateway.", output.cpf());

    return output;
  }

  private boolean verifyIfUserExists(String cpf) {
    return FindByCPFUseCase.create(this.userGateway)
            .execute(new FindByCPFCommand(cpf)) != null;
  }

  private User createUser(CreateUserCommand createUserCommand) {
    final String name = createUserCommand.name();
    final String cpf = createUserCommand.cpf();

    log.info("creating new user with name: '{}' and cpf: '{}'.", name, cpf);

    User user = User.create(name, cpf);
    log.info("user created with id: '{}'.", user.getId().getValue());

    return user;
  }

}
