package com.desafio.application.user.findAll;

import com.desafio.application.user.common.CommonUserOutput;
import com.desafio.crosscutting.log.ILog;
import com.desafio.crosscutting.log.Log;
import com.desafio.domain.user.User;
import com.desafio.domain.user.UserGateway;

import java.util.List;
import java.util.stream.Collectors;

public class FindAllUsersUseCase {

  private static final ILog log = new Log(FindAllUsersUseCase.class);

  private final UserGateway userGateway;

  private FindAllUsersUseCase(UserGateway userGateway) {
    this.userGateway = userGateway;
  }

  public static FindAllUsersUseCase create(UserGateway userGateway) {
    return new FindAllUsersUseCase(userGateway);
  }

  public List<CommonUserOutput> execute() throws RuntimeException {
    log.info("Getting all users");

    List<User> users = this.userGateway.findAll();
    log.info("Got {} users", users.size());
    return users.stream().map(CommonUserOutput::from).collect(Collectors.toList());
  }
}
