package com.desafio.application.user.findByCPF;

public record FindByCPFCommand(String cpf) {

  public static FindByCPFCommand with(String cpf) {
    return new FindByCPFCommand(cpf);
  }

}
