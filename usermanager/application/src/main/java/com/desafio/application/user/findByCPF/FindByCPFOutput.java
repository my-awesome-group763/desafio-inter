package com.desafio.application.user.findByCPF;

import com.desafio.application.user.create.CreateUserOutput;
import com.desafio.crosscutting.notification.INotification;
import com.desafio.domain.user.User;

public record FindByCPFOutput(String name, String cpf, INotification notificationErrors) {

  public static FindByCPFOutput from(User user) {
    return new FindByCPFOutput(user.getName(), user.getCpf().getValue(), null);
  }

  public static FindByCPFOutput from(INotification notification) {
    return new FindByCPFOutput(null, null, notification);
  }

  public FindByCPFOutput(String name, String cpf) {
    this(name, cpf, null);
  }
}