package com.desafio.application.user.findByCPF;

import com.desafio.application.user.common.CommonUserOutput;
import com.desafio.application.user.findAll.FindAllUsersUseCase;
import com.desafio.application.utils.Constants;
import com.desafio.crosscutting.log.ILog;
import com.desafio.crosscutting.log.Log;
import com.desafio.crosscutting.notification.Notification;
import com.desafio.crosscutting.notification.NotificationErrorProps;
import com.desafio.domain.user.User;
import com.desafio.domain.user.UserGateway;

import java.util.Optional;

public class FindByCPFUseCase {

  private static final ILog log = new Log(FindAllUsersUseCase.class);

  private final UserGateway userGateway;

  private FindByCPFUseCase(UserGateway userGateway) {
    this.userGateway = userGateway;
  }

  public static FindByCPFUseCase create(UserGateway userGateway) {
    return new FindByCPFUseCase(userGateway);
  }

  public FindByCPFOutput execute(FindByCPFCommand command) throws IllegalStateException {
    log.info("Getting user by CPF {}...", command.cpf());

    Optional<User> actualUser = this.userGateway.findByCPF(command.cpf());

    if(actualUser.isEmpty())
      return this.buildErrorNotification(command.cpf());

    return actualUser.map(FindByCPFOutput::from).get();
  }

  private FindByCPFOutput buildErrorNotification(String cpf) {
    String message = Constants.USER_ALREADY_EXISTS.replace("{}", cpf);
    log.info(message);
    Notification notification = new Notification();
    NotificationErrorProps userAlreadyExists = new NotificationErrorProps(message, Constants.CLASS_STR);
    notification.append(userAlreadyExists);
    return FindByCPFOutput.from(notification);
  }
}
