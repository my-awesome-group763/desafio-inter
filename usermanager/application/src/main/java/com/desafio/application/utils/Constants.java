package com.desafio.application.utils;

public class Constants {
  public static final String CLASS_STR = "USER";
  public static final String USER_ALREADY_EXISTS = "User with cpf: {} already exists.";
  public static final String USER_VALIDATED = "object User '{}' successfully validated.";
}
