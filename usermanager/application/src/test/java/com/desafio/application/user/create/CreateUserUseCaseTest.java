package com.desafio.application.user.create;

import com.desafio.application.utils.TestConstants;
import com.desafio.domain.user.User;
import com.desafio.domain.user.UserGateway;
import com.desafio.domain.user.cpf.CPF;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Objects;
import java.util.Optional;

import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CreateUserUseCaseTest {

  @Mock
  private UserGateway userGateway;

  @InjectMocks
  private CreateUserUseCase useCase;

  @Test
  void givenValidCommand_whenCallsCreateCategory_shouldReturnCategoryId() {
    final var command = CreateUserCommand.with(TestConstants.NAME_SAMPLE, TestConstants.CPF_SAMPLE);

    when(userGateway.create(any())).thenAnswer(returnsFirstArg());

    final CreateUserOutput output = useCase.execute(command);

    Assertions.assertNotNull(output);
    Assertions.assertNotNull(output.cpf());

    verify(userGateway, times(1)).create(Mockito.argThat(user ->
            Objects.equals(TestConstants.NAME_SAMPLE, user.getName()) && Objects.equals(CPF.create(TestConstants.CPF_SAMPLE), user.getCpf())
    ));
  }

  @Test
  void givenValidCommandAndExistentUser_whenCallsCreateCategory_shouldReturnCategoryId() {
    final var command = CreateUserCommand.with(TestConstants.NAME_SAMPLE, TestConstants.CPF_SAMPLE);

    when(userGateway.findByCPF(any())).thenReturn(
            Optional.of(User.create(TestConstants.NAME_SAMPLE, TestConstants.CPF_SAMPLE))
    );

    final var exception = Assertions.assertThrows(RuntimeException.class, () -> useCase.execute(command));

    verify(userGateway, times(1)).findByCPF(any());
  }

  @Test
  void givenInvalidBlankName_whenCallsCreateCategory_shouldThrowAnError() {
    final String expected_error_message = "[USER]: 'NAME' should not be blank.[USER]: 'NAME' must be between 3 and 30 chars.";

    final var command = CreateUserCommand.with("", TestConstants.CPF_SAMPLE);
    final var output = useCase.execute(command);

    Assertions.assertEquals(expected_error_message, output.notificationErrors().messages(""));
    verify(userGateway, times(0)).create(any());
  }

  @Test
  void givenValidCommand_whenGatewayThrowsAnError_shouldThrowAnError() {

    final var command = CreateUserCommand.with(TestConstants.NAME_SAMPLE, TestConstants.CPF_SAMPLE);

    when(userGateway.create(any())).thenThrow(new RuntimeException(TestConstants.GATEWAY_ERROR_STR));

    final var exception = Assertions.assertThrows(RuntimeException.class, () -> useCase.execute(command));

    verify(userGateway, times(1)).create(any());
  }
}
