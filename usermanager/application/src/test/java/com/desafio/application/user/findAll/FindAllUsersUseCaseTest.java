package com.desafio.application.user.findAll;

import com.desafio.application.utils.TestConstants;
import com.desafio.application.user.common.CommonUserOutput;
import com.desafio.domain.user.User;
import com.desafio.domain.user.UserGateway;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class FindAllUsersUseCaseTest {

  @Mock
  private UserGateway userGateway;

  @InjectMocks
  private FindAllUsersUseCase useCase;

  @Test
  public void givenValidQuery_whenCallAllUsers_thenShouldReturnListOfUsers() {
    final List<User> expected_users = List.of(User.create(
            TestConstants.NAME_SAMPLE, TestConstants.CPF_SAMPLE),
            User.create(TestConstants.NAME_SAMPLE, TestConstants.CPF_SAMPLE)
    );

    final Integer expected_item_count = expected_users.size();

    when(userGateway.findAll()).thenReturn(expected_users);

    List<CommonUserOutput> outputList = useCase.execute();

    assertEquals(expected_item_count, outputList.size());
  }

  @Test
  public void givenValidQuery_whenHasNoUsers_thenShouldReturnEmptyListOfUsers() {
    final List<User> expected_users = new ArrayList<>();
    final Integer expected_item_count = 0;

    when(userGateway.findAll()).thenReturn(expected_users);

    List<CommonUserOutput> outputList = useCase.execute();

    assertEquals(expected_item_count, outputList.size());
  }

  @Test
  public void givenValidQuery_whenGatewayThrowsException_thenShouldThrowException() {
    when(userGateway.findAll()).thenThrow(new RuntimeException(TestConstants.GATEWAY_ERROR_STR));

    Assertions.assertThrows(RuntimeException.class, () -> useCase.execute());

    verify(userGateway, times(1)).findAll();
  }
}
