package com.desafio.application.user.findByCPF;

import com.desafio.application.utils.TestConstants;
import com.desafio.application.user.common.CommonUserOutput;
import com.desafio.domain.user.User;
import com.desafio.domain.user.UserGateway;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class FindByCPFUseCaseTest {

  @Mock
  private UserGateway userGateway;

  @InjectMocks
  private FindByCPFUseCase useCase;

  @Test
  void givenValidCPF_whenCallsFindByCPF_shouldReturnUser() {
    final Optional<User> expected_user = Optional.of(User.create(TestConstants.NAME_SAMPLE, TestConstants.CPF_SAMPLE));
    FindByCPFCommand command = FindByCPFCommand.with(TestConstants.CPF_SAMPLE);

    when(userGateway.findByCPF(anyString())).thenReturn(expected_user);

    CommonUserOutput output = useCase.execute(command);

    Assertions.assertNotNull(output);
    Assertions.assertNotNull(output.id());
  }

  @Test
  void givenValidCPF_whenGatewayThrowException_shouldThrowException() {
    FindByCPFCommand command = FindByCPFCommand.with(TestConstants.CPF_SAMPLE);

    when(userGateway.findByCPF(command.cpf())).thenThrow(new IllegalStateException(TestConstants.GATEWAY_ERROR_STR));

    final var exception = Assertions.assertThrows(IllegalStateException.class, () -> useCase.execute(command));

    verify(userGateway, times(1)).findByCPF(command.cpf());
  }

  @Test
  void givenInvalidCPF_whenCallsFindByCPF_shouldReturnNothing() {
    FindByCPFCommand command = FindByCPFCommand.with("123");

    when(userGateway.findByCPF(anyString())).thenReturn(Optional.empty());

    CommonUserOutput output = useCase.execute(command);

    Assertions.assertNull(output);
  }
}
