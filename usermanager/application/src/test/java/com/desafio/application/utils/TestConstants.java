package com.desafio.application.utils;

public class TestConstants {
  public static final String NAME_SAMPLE = "Thales";
  public static final String CPF_SAMPLE = "68650110070"; //https://www.4devs.com.br/gerador_de_cpf
  public static final String GATEWAY_ERROR_STR = "Gateway error";
}
