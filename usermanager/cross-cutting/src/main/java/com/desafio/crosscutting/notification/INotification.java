package com.desafio.crosscutting.notification;

import java.util.ArrayList;

public interface INotification {
  void append(NotificationErrorProps error);
  boolean hasErrors();
  ArrayList<NotificationErrorProps> getErrors();
  String messages(String context);
}