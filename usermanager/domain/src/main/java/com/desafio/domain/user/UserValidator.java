package com.desafio.domain.user;

import com.desafio.domain.Validator;
import com.desafio.domain.user.cpf.ICPFValidator;
import com.desafio.domain.user.handler.afterValidation.IAfterValidation;
import com.desafio.domain.utils.Constants;
import com.desafio.crosscutting.notification.NotificationErrorProps;

import java.util.List;

public class UserValidator extends Validator<User> {

  private final ICPFValidator cpfValidator;

  private final List<IAfterValidation<User>> validationHandlers;
  public UserValidator(ICPFValidator cpfValidator, List<IAfterValidation<User>> validationHandlers) {
    this.cpfValidator = cpfValidator;
    this.validationHandlers = validationHandlers;
  }

  @Override
  public void validate(User user) {
    this.checkNameConstraints(user);
    this.cpfValidator.validate(user.getCpf(), user.getNotification());

    if(!user.getNotification().hasErrors())
      this.validationHandlers.forEach(handler -> handler.execute(user));
  }

  private void checkNameConstraints(User user) {
    if (user.getName() == null) {
      user.getNotification().append(new NotificationErrorProps(String.format(Constants.STRING_SHOULD_NOT_BE_NULL, Constants.NAME_STR), Constants.CLASS_STR));
      return;
    } else if (user.getName().isEmpty()) {
      user.getNotification().append(new NotificationErrorProps(String.format(Constants.STRING_SHOULD_NOT_BE_BLANK, Constants.NAME_STR), Constants.CLASS_STR));
    }

    final String name = user.getName().trim();
    if (name.length() < Constants.MIN_NAME_LEN || name.length() > Constants.MAX_NAME_LEN)
      user.getNotification().append(new NotificationErrorProps(Constants.NAME_LENGTH_INVALID, Constants.CLASS_STR));
  }

}
