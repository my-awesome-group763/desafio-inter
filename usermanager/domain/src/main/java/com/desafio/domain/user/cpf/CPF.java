package com.desafio.domain.user.cpf;

import com.desafio.domain.ValueObject;

import java.util.Objects;

public class CPF extends ValueObject {

  private final String value;

  private CPF(String value) {
    this.value = value;
  }

  public static CPF create(String value) {
    return new CPF(value);
  }

  public static CPF format(String value) {
    return new CPF(value.trim().replaceAll("[^\\d ]", ""));
  }

  public String getValue() {
    return value;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    CPF cpf = (CPF) o;
    return getValue().equals(cpf.getValue());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getValue());
  }
}
