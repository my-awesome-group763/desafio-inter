package com.desafio.domain.user.cpf;


import com.desafio.crosscutting.notification.INotification;

public interface ICPFValidator {

  // https://gist.github.com/igorcosta/3a4caa954a99035903ab
  String CPF_REGEX = "[0-9]{3}[.]?[0-9]{3}[.]?[0-9]{3}[-]?[0-9]{2}";
  public abstract void validate(CPF cpf, INotification notification);

}
