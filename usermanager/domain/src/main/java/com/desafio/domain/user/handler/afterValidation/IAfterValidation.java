package com.desafio.domain.user.handler.afterValidation;

public interface IAfterValidation<T> {
  void execute(T object);
}
