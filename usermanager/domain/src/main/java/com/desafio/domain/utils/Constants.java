package com.desafio.domain.utils;

public class Constants {
  public static final Integer MAX_NAME_LEN = 30;
  public static final Integer MIN_NAME_LEN = 3;

  public static final String STRING_SHOULD_NOT_BE_NULL = "'%s' should not be null.";
  public static final String STRING_SHOULD_NOT_BE_BLANK = "'%s' should not be blank.";
  public static final String NAME_LENGTH_INVALID = "'NAME' must be between 3 and 30 chars.";
  public static final String CPF_INVALID_FORMAT = "'CPF' invalid format %S.";
  public static final String CPF_INVALID = "'CPF' is invalid %S.";

  public static final String NAME_STR = "NAME";
  public static final String CPF_STR = "CPF";
  public static final String CLASS_STR = "USER";
}
