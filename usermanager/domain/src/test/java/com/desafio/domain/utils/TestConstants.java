package com.desafio.domain.utils;

public class TestConstants {
  public static final String NAME_SAMPLE = "Thales";
  public static final String CPF_SAMPLE = "68650110070"; //https://www.4devs.com.br/gerador_de_cpf
  public static final String FORMATED_CPF_SAMPLE = "686.501.100-70";
  public static final String CPF_STR = "CPF";
  public static final String NAME_STR = "NAME";
  public static final String LERO_LERO = """
      Todavia, a estrutura atual da organização exige a
      precisão e a definição dos métodos utilizados na avaliação de resultados."""; //https://lerolero.com/

}
