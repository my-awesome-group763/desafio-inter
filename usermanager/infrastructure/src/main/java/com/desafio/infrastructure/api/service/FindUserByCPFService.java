package com.desafio.infrastructure.api.service;

import com.desafio.application.user.findByCPF.FindByCPFCommand;
import com.desafio.application.user.findByCPF.FindByCPFOutput;
import com.desafio.application.user.findByCPF.FindByCPFUseCase;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.net.URI;

@Component
public class FindUserByCPFService {

  private final FindByCPFUseCase useCase;

  public FindUserByCPFService(FindByCPFUseCase useCase) {
    this.useCase = useCase;
  }

  public FindByCPFOutput execute(String cpf) {
    final var command = FindByCPFCommand.with(cpf);
    return this.useCase.execute(command);
  }
}
