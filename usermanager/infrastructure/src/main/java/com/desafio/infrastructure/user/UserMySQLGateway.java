package com.desafio.infrastructure.user;

import com.desafio.domain.user.User;
import com.desafio.domain.user.UserGateway;
import com.desafio.infrastructure.user.persistence.UserJPAEntity;
import com.desafio.infrastructure.user.persistence.UserRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class UserMySQLGateway implements UserGateway {

  private final UserRepository userRepository;

  public UserMySQLGateway(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public User create(User user) {
    return this.userRepository.save(UserJPAEntity.from(user)).toAggregate();
  }

  @Override
  public List<User> findAll() {
    return this.userRepository.findAll().stream().map(UserJPAEntity::toAggregate).collect(Collectors.toList());
  }

  @Override
  public Optional<User> findByCPF(String cpf) {
    return this.userRepository.findByCpf(cpf).map(UserJPAEntity::toAggregate);
  }
}
