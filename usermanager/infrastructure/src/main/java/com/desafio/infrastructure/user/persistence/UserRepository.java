package com.desafio.infrastructure.user.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserJPAEntity, String> {

  Optional<UserJPAEntity> findByCpf(String cpf);

}
