package com.desafio.infrastructure.user.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public record CreateUserRequest(@NotNull @NotBlank String name, @NotNull @NotBlank String cpf) {
  public static CreateUserRequest create(String name, String cpf) {
    return new CreateUserRequest(name, cpf);
  }
}
