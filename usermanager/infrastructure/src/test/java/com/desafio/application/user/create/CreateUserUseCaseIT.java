package com.desafio.application.user.create;

import com.desafio.application.user.AbstractIT;
import com.desafio.utils.TestConstants;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class CreateUserUseCaseIT extends AbstractIT {

  @Autowired
  CreateUserUseCase useCase;

  @Test
  void givenValidCommand_whenCallsCreateUser_shouldReturnUserId() {

    Assertions.assertEquals(0, repository.count());

    final var aCommand =
            CreateUserCommand.with(TestConstants.NAME_SAMPLE, TestConstants.CPF_SAMPLE);

    final var actualOutput = useCase.execute(aCommand);

    Assertions.assertNotNull(actualOutput);
    Assertions.assertEquals(TestConstants.CPF_SAMPLE, actualOutput.cpf());

    Assertions.assertEquals(1, repository.count());

    final var actualUser =
            repository.findByCpf(actualOutput.cpf()).get();

    Assertions.assertEquals(TestConstants.NAME_SAMPLE, actualUser.getName());
    Assertions.assertEquals( TestConstants.CPF_SAMPLE, actualUser.getCpf());
    Assertions.assertNotNull(actualUser.getCreated_at());
    Assertions.assertNotNull(actualUser.getId());
  }

}
