package com.desafio.application.user.findByCPF;

import com.desafio.application.user.AbstractIT;
import com.desafio.domain.user.User;
import com.desafio.infrastructure.user.persistence.UserJPAEntity;
import com.desafio.utils.TestConstants;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class FindByCPFUseCaseIT extends AbstractIT {

  @Autowired
  FindByCPFUseCase useCase;

  @Test
  void givenValidCommand_whenCallsFindByCPF_shouldReturnUser() {

    Assertions.assertEquals(0, repository.count());


    final var user = User.create(TestConstants.NAME_SAMPLE, TestConstants.CPF_SAMPLE);

    final var userOutput = this.repository.save(UserJPAEntity.from(user));

    Assertions.assertNotNull(userOutput);
    Assertions.assertNotNull(userOutput.getId());

    Assertions.assertEquals(1, repository.count());

    final var aCommand =
            FindByCPFCommand.with(TestConstants.CPF_SAMPLE);

    final var actualUser = useCase.execute(aCommand);

    Assertions.assertEquals(TestConstants.NAME_SAMPLE, actualUser.name());
    Assertions.assertEquals(TestConstants.CPF_SAMPLE, actualUser.cpf());
    Assertions.assertNull(actualUser.notificationErrors());
  }

}
