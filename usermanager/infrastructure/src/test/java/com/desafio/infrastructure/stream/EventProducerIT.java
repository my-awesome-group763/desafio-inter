package com.desafio.infrastructure.stream;

import com.desafio.IntegrationTest;
import com.desafio.application.user.create.CreateUserUseCase;
import com.desafio.infrastructure.api.service.CreateUserService;
import com.desafio.infrastructure.user.request.CreateUserRequest;
import com.desafio.utils.TestConstants;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Map;

@IntegrationTest
public class EventProducerIT extends KafkaConfigTestContainers {

    @Autowired
    private CreateUserService createUserService;

    @Autowired
    private CreateUserUseCase useCase;

    @Autowired
    private EventProducer eventProducer;

    private static final ObjectMapper mapper = new ObjectMapper();

    @Test
    void givenAValidCommand_whenCallsCreateUserEndpoint_itProduceAnEvent() throws Exception {
      final var requestBody = CreateUserRequest.create(TestConstants.NAME_SAMPLE, TestConstants.CPF_SAMPLE);


      createUserService.execute(TestConstants.NAME_SAMPLE, TestConstants.CPF_SAMPLE);
      ConsumerRecord<String, String> singleRecord =
              KafkaTestUtils.getSingleRecord(
                      kafkaConsumer, TestConstants.EVENT_NAME, TestConstants.KAFKA_TIMEOUT);
      Assertions.assertNotNull(singleRecord);

      Map<String, String> output = mapper.readValue(singleRecord.value(), Map.class);
      Assertions.assertEquals(output.get("cpf"), TestConstants.CPF_SAMPLE);
    }

  @Test
  void givenInvalidCommandNullName_whenCallsCreateUserEndpoint_itDoesNothing() throws Exception {
    String expectedException = "No records found for topic";

    createUserService.execute(null, TestConstants.CPF_SAMPLE);

    final var actualException =
            Assertions.assertThrows(
                    IllegalStateException.class,
                    () -> KafkaTestUtils.getSingleRecord(kafkaConsumer, TestConstants.EVENT_NAME, TestConstants.KAFKA_TIMEOUT));

    Assertions.assertEquals(expectedException, actualException.getMessage());
  }

}
