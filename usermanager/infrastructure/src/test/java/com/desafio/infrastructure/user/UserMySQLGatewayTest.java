package com.desafio.infrastructure.user;

import com.desafio.domain.user.User;
import com.desafio.MySQLGatewayTest;
import com.desafio.infrastructure.user.persistence.UserJPAEntity;
import com.desafio.infrastructure.user.persistence.UserRepository;
import com.desafio.utils.TestConstants;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.wildfly.common.Assert;

import java.util.List;

@MySQLGatewayTest
public class UserMySQLGatewayTest {
  @Autowired
  private UserMySQLGateway mySQLGateway;

  @Autowired
  private UserRepository userRepository;

  @BeforeEach
  void setup() {
    userRepository.deleteAll();
  }

  private static final User user = User.create(TestConstants.NAME_SAMPLE, TestConstants.CPF_SAMPLE);

  @Test
  void givenAValidUser_whenCallsCreate_itShouldReturnNewInstance() {

    Assertions.assertEquals(0, userRepository.count());

    User created_user = mySQLGateway.create(user);

    Assertions.assertEquals(user.getName(), created_user.getName());
    Assertions.assertEquals(user.getCpf(), created_user.getCpf());
    Assertions.assertEquals(user.getId(), created_user.getId());
    Assertions.assertEquals(user.getCreatedAt(), created_user.getCreatedAt());

    final var actualUser = userRepository.findById(created_user.getId().getValue()).get();

    Assertions.assertEquals(actualUser.getName(), created_user.getName());
    Assertions.assertEquals(actualUser.getCpf(), created_user.getCpf().getValue());
    Assertions.assertEquals(actualUser.getId(), created_user.getId().getValue());
    Assertions.assertEquals(actualUser.getCreated_at(), created_user.getCreatedAt());

  }

  @Test
  void givenPrePersistedUserAndValidUserId_whenCallsFindById_itShouldReturnInstance() {

    Assertions.assertEquals(0, userRepository.count());

    userRepository.saveAndFlush(UserJPAEntity.from(user));

    Assertions.assertEquals(1, userRepository.count());

    final var actualUser = mySQLGateway.findByCPF(user.getCpf().getValue());

    Assertions.assertEquals(actualUser.get(), user);

  }


  @Test
  void givenPrePersistedUserAndValidCpf_whenCallsFindByCpf_itShouldReturnInstance() {

    Assertions.assertEquals(0, userRepository.count());

    userRepository.saveAndFlush(UserJPAEntity.from(user));

    Assertions.assertEquals(1, userRepository.count());

    final var actualUser = mySQLGateway.findByCPF(user.getCpf().getValue());

    Assertions.assertNotNull(actualUser);
    Assertions.assertEquals(actualUser.get(), user);

  }

  @Test
  public void givenPrePersistedCategories_whenCallsFindAll_shouldReturnPaginated() {

    Assertions.assertEquals(0, userRepository.count());

    userRepository.saveAll(List.of(
            UserJPAEntity.from(user)
    ));

    Assertions.assertEquals(1, userRepository.count());

    final var actualResult = mySQLGateway.findAll();

    Assertions.assertEquals(1, actualResult.size());
    Assertions.assertEquals(user.getId().getValue(), actualResult.get(0).getId().getValue());
  }
}
