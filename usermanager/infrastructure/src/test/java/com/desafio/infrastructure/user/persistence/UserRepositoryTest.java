package com.desafio.infrastructure.user.persistence;

import com.desafio.MySQLGatewayTest;
import com.desafio.domain.user.User;
import com.desafio.utils.TestConstants;
import org.hibernate.PropertyValueException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

@MySQLGatewayTest
public class UserRepositoryTest {

  @Autowired
  private UserRepository userRepository;

  @BeforeEach
  void setup() {
    userRepository.deleteAll();
  }

  private static final User user = User.create(TestConstants.NAME_SAMPLE, TestConstants.CPF_SAMPLE);

  @Test
  void givenInvalidNullName_whenCallsSave_shouldReturnError() {

    final var userJpaEntity = UserJPAEntity.from(user);
    userJpaEntity.setName(null);

    final var actualException = Assertions.assertThrows(DataIntegrityViolationException.class, () -> userRepository.save(userJpaEntity));

    final var actualCause = Assertions.assertInstanceOf(PropertyValueException.class, actualException.getCause());

    Assertions.assertEquals("name", actualCause.getPropertyName());
    Assertions.assertEquals("not-null property references a null or transient value : com.desafio.infrastructure.user.persistence.UserJPAEntity.name", actualCause.getMessage());
  }

  @Test
  void givenInvalidNullCreatedAt_whenCallsSave_shouldReturnError() {

    final var userJpaEntity = UserJPAEntity.from(user);
    userJpaEntity.setCreated_at(null);

    final var actualException = Assertions.assertThrows(DataIntegrityViolationException.class, () -> userRepository.save(userJpaEntity));

    final var actualCause = Assertions.assertInstanceOf(PropertyValueException.class, actualException.getCause());

    Assertions.assertEquals("created_at", actualCause.getPropertyName());
    Assertions.assertEquals("not-null property references a null or transient value : com.desafio.infrastructure.user.persistence.UserJPAEntity.created_at", actualCause.getMessage());
  }

}
