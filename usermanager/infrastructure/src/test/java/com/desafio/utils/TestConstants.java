package com.desafio.utils;

public class TestConstants {
  public static final String NAME_SAMPLE = "Thales";
  public static final String CPF_SAMPLE = "68650110070"; //https://www.4devs.com.br/gerador_de_cpf
  public static final String ROUTE_USERS = "/users";

  public static final String NAME_STR = "NAME";
  public static final String USER_STR = "USER";
  public static final String STRING_SHOULD_NOT_BE_NULL = "'%s' should not be null.";
  public static final String STRING_SHOULD_NOT_BE_BLANK = "'%s' should not be blank.";
  public static final String NAME_LENGTH_INVALID = "'NAME' must be between 3 and 30 chars.";
  public static final String EVENT_NAME = "USER_CREATED";
  public static final long KAFKA_TIMEOUT = 5000;
}
